import { Interaction } from "../interfaces/interaction.ts";
import { Logger, Discord, InteractionResponseTypes, ApplicationCommandFlags } from "../../deps.ts";
import { getOptionValue } from "../util/get-option-value.ts";

export class ChannelInteraction implements Interaction {
  private interaction: any;

  public constructor(opts: any) {
    this.interaction = opts.interaction;
  }

  public async execute(): Promise<void> {
    // Find the channelid
    const channel = getOptionValue('channel', this.interaction.data.options);

    // Send the message
    try {
      await Discord.getBot().helpers.sendInteractionResponse(
        this.interaction.id,
        this.interaction.token,
        {
          type: InteractionResponseTypes.ChannelMessageWithSource,
          data: {
            flags: ApplicationCommandFlags.Ephemeral,
            embeds: [
              {
                color: 8263,
                title: 'Das Propaganda Maschine - Channel',
                description: `You can easily move to the desired channel by tapping here:\r\n<#${channel}>`
              }
            ]
          }
        }
      );
    } catch(e) {
      Logger.error(`Could not send channel to user: "${e.message}"`, e.stack);
    }
  }
}
