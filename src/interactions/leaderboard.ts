import { Logger, InteractionResponseTypes, Discord, Time } from "../../deps.ts";
import { Interaction } from "../interfaces/interaction.ts";
import { Leaderboard } from "../leaderboard.ts";
import { getOptionValue } from "../util/get-option-value.ts";
import { Scores, Schema as ScoreEntry } from "../schema/scores.ts";

export class LeaderboardInteraction implements Interaction {
  private interaction: any;

  public constructor(opts: any) {
    this.interaction = opts.interaction;
  }

  /**
   * Main logic for this interaction:
   *
   * @returns Promise<void>
   */
  public async execute(): Promise<void> {
    // Get the current top 10
    const top10 = Leaderboard.top10();

    // Build our embed fields
    let users = '';
    let totalScores = '';
    for(const index in top10) {
      users += `\`${parseInt(index) + 1}\` <@${top10[index].snowflake}>\n`;
      totalScores += `\`${top10[index].total_score}\`\n`;
    }

    // Add own (or specified user) data
    // If user is on excluded list, lookup separately but do not assign position
    let id = getOptionValue('user', this.interaction.data.options);
    if(id) {
      id = id.toString();
    } else {
      id = this.interaction.user.id.toString();
    }
    if(!Leaderboard.exclusions.includes(id)) {
      const target = Leaderboard.find(id);
      if(target) {
        users += `...\n\`${target.position}\` <@${target.id}>\n`;
        totalScores += `\n\`${target.data.total_score}\``;
      }
    } else {
      const score: ScoreEntry = await Scores().findOne({ snowflake: id }) as ScoreEntry;
      users += `...\n\`E\` <@${id}>\n`;
      totalScores += `\n\`${score.total_score}\``;
    }

    // Send our leaderboard
    try {
      let updatedTime = 'Unknown'
      if(Leaderboard.lastUpdated) updatedTime = new Time(Leaderboard.lastUpdated!.toISOString()).format('yyyy/MM/dd HH:mm:ss');
      await Discord.getBot().helpers.sendInteractionResponse(
        this.interaction.id,
        this.interaction.token,
        {
          type: InteractionResponseTypes.ChannelMessageWithSource,
          data: {
            embeds: [
              {
                color: 8263,
                title: `Das Propaganda Maschine - Leaderboard`,
                fields: [
                  {name: 'Top 10', value: users, inline: true},
                  {name: 'Total Score', value: totalScores, inline: true},
                ],
                timestamp: Leaderboard.lastUpdated!.getTime(),
                footer: {
                  text: `Last Update`,
                }
              }
            ],
          }
        }
      );
    } catch(e) {
      Logger.error(`Could not send leaderboard: "${e.message}"`, e.stack);
    }
  }

  private async findRunner() {

  }
}
