import { Interaction } from "../interfaces/interaction.ts";
import { Logger, Discord, InteractionResponseTypes, ApplicationCommandFlags } from "../../deps.ts";
import { getOptionValue } from "../util/get-option-value.ts";

export class SayInteraction implements Interaction {
  private interaction: any;

  public constructor(opts: any) {
    this.interaction = opts.interaction;
  }

  public async execute(): Promise<void> {
    // Make sure the command is issued by Teitoku
    if(this.interaction.user.id !== 91616138860978176n) {
      await Discord.getBot().helpers.sendInteractionResponse(
        this.interaction.id,
        this.interaction.token,
        {
          type: InteractionResponseTypes.ChannelMessageWithSource,
          private: true,
          data: {
            embeds: [
              {
                color: 15158332,
                title: 'Say - Error',
                description: `This command may only be executed by Teitoku.`
              }
            ]
          }
        }
      );
      return;
    }

    // Find the channelid and make sure the channel exists
    const channelId = this.getChannel();

    // Get the message to send
    const message = getOptionValue('message', this.interaction.data.options);
    if(!message) {
      await Discord.getBot().helpers.sendInteractionResponse(
        this.interaction.id,
        this.interaction.token,
        {
          type: InteractionResponseTypes.ChannelMessageWithSource,
          data: {
            flags: ApplicationCommandFlags.Ephemeral,
            embeds: [
              {
                color: 15158332,
                title: 'Say - Error',
                description: `A message must be specified. (this is a bug!)`
              }
            ]
          }
        }
      );
      return;
    }

    // Send the message
    try {
      await Discord.getBot().helpers.sendMessage(channelId, {
        content: message
      });

      await Discord.getBot().helpers.sendInteractionResponse(
        this.interaction.id,
        this.interaction.token,
        {
          type: InteractionResponseTypes.ChannelMessageWithSource,
          data: {
            flags: ApplicationCommandFlags.Ephemeral,
            embeds: [
              {
                color: 8263,
                title: 'Say',
                description: `Say executed!`
              }
            ]
          }
        }
      );
    } catch(e) {
      Logger.error(`Could not send message in channel: "${e.message}"`, e.stack);
    }
  }

  private getChannel(): BigInt {
    const channel = getOptionValue('channel', this.interaction.data.options);
    return channel ?? this.interaction.channelId;
  }
}
