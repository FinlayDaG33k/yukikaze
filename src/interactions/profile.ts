import { Logger, InteractionResponseTypes, Time, Discord } from "../../deps.ts";
import { calculateScore } from "../util/calculateScore.ts";
import { calculateGiveawayPower } from "../util/calculateGiveawayPower.ts";
import { getMembershipDays } from "../util/get-membership-days.ts";
import { Interaction } from "../interfaces/interaction.ts";
import { GuildMember, Schema as GuildMemberEntry } from "../schema/guild-member.ts";
import { ChannelMemberMessages, Schema as ChannelMemberMessagesEntry } from "../schema/channel-member-messages.ts";
import { snowflakeToDate } from "../util/snowflake-to-date.ts";
import { dateMention } from "../util/date-mention.ts";

export class ProfileInteraction implements Interaction {
  private interaction: any;

  public constructor(opts: any) {
    this.interaction = opts.interaction;
  }

  /**
   * Main logic for this interaction:
   * - Obtains the user profile for the specified user (defaults to sender if none specified)
   *
   * @returns Promise<void>
   */
  public async execute(): Promise<void> {
    // Get the user information
    try {
      const user = await this.getUserInfo();

      // Get all required data
      const [favourites, lastSeen] = await Promise.all([
        this.getFavouriteChannels(user.id),
        ProfileInteraction.getLastSeen(user.id)
      ]);

      // Calculate a user's score
      const score = calculateScore(0, user.messages, getMembershipDays(user.joinedAt, lastSeen));

      // Send the profile
      Discord.getBot().helpers.sendInteractionResponse(
        this.interaction.id,
        this.interaction.token,
        {
          type: InteractionResponseTypes.ChannelMessageWithSource,
          data: {
            embeds: [
              {
                color: 8263,
                title: `Profile - ${user.username}`,
                thumbnail: {
                  url: user.avatar
                },
                description: `
                **Messages:** ${user.messages}
                **Score:** ${score}
                **Giveaway Power:** ${calculateGiveawayPower(score)}`,
                fields: [
                  {name: 'Favourite Channels:', value: favourites, inline: false},
                  {name: 'Account Creation:', value: (ProfileInteraction.getCreationDate(user.id?.toString())), inline: true},
                  {name: 'Joined On:', value: (ProfileInteraction.parseJoinedAt(user.joinedAt)), inline: true},
                  {name: 'Last Seen:', value: dateMention(new Date(lastSeen), 'R'), inline: true}
                ]
              }
            ]
          }
        }
      );
    } catch(e) {
      Logger.error(`Could not send profile: "${e.message}"`, e.stack);
    }
  }

  /**
   * Get a workable object containing the user information.
   * TODO: Make an interface for the expected data
   *
   * @returns Promise<any>
   */
  private async getUserInfo(): Promise<any> {
    // Get the from the interaction
    // If no user is specified, just return the issuer
    if(typeof this.interaction.data.options === 'undefined') {
      return {
        username: this.interaction.member.nick || this.interaction.user.username || "UNKNOWN USER",
        id: this.interaction.user.id,
        avatar: Discord.getBot().helpers.getAvatarURL(this.interaction.user.id, this.interaction.user.discriminator, {avatar: this.interaction.member.avatar || this.interaction.user.avatar}),
        joinedAt: this.interaction.member.joinedAt,
        messages: await ProfileInteraction.getMessageCount(this.interaction.user.id)
      }
    }

    // Find the id of the user requested
    const req = this.interaction.data.options.find((option: any) => option.name === 'user');
    const userId = BigInt(req.value);

    // Get the resolved user and member
    const resolvedUser = this.interaction.data.resolved.users.get(userId);
    const resolvedMember = this.interaction.data.resolved.members.get(userId);

    return {
      username: resolvedMember.nick || resolvedUser.username || "UNKNOWN USER",
      id: userId,
      avatar: Discord.getBot().helpers.getAvatarURL(userId, resolvedUser.discriminator, {avatar: resolvedMember.avatar || resolvedUser.avatar}),
      joinedAt: resolvedMember.joinedAt,
      messages: await ProfileInteraction.getMessageCount(userId)
    }
  }

  /**
   * Get the message count for a user by its ID
   *
   * @param id Discord snowflake for the user
   * @returns Promise<number>
   */
  private static async getMessageCount(id: any): Promise<number> {
    const entry: GuildMemberEntry = await GuildMember().findOne({
      snowflake: id.toString()
    }) as GuildMemberEntry;
    if(!entry) return 0;
    return entry.messages ?? 0;
  }

  /**
   *  Get the last seen for the user formatted as "yyyy/MM/dd HH:mm:ss".
   *  Returns "Never" when the user has not sent any messages yet
   *
   * @param id Discord snowflake for the user
   * @returns Promise<string>
   */
  private static async getLastSeen(id: any): Promise<string> {
    const entry: GuildMemberEntry = await GuildMember().findOne({
      snowflake: id.toString()
    }) as GuildMemberEntry;
    if(!entry) return "Never";
    return new Time(entry.last_seen.toISOString()).format(`yyyy/MM/dd HH:mm:ss`);
  }

  /**
   *  Get a list of favourite channels and the amount of messages for the user
   *
   * @param id Discord snowflake for the user
   * @returns Promise<string>
   */
  private async getFavouriteChannels(id: any): Promise<string> {
    // Get the channels, sort them and get the top 3
    const channels: ChannelMemberMessagesEntry[] = await ChannelMemberMessages().find({
      member: id.toString(),
    }, { sort: { count: -1 }, limit: 3 }) as ChannelMemberMessagesEntry[];

    // Build our message
    let message = '';
    for await(const channel of channels) {
      message += `<#${channel.channel}> (${channel.count})\r\n`;
    }

    return message;
  }

  /**
   *  Get the join date for the user formatted as "yyyy/MM/dd HH:mm:ss".
   *
   * @param timestamp Unix timestamp of when the user joined
   * @returns string
   */
  private static parseJoinedAt(timestamp: number): string {
    // Format the time
    return dateMention(new Date(timestamp), 'f');
  }

  /**
   * Get the date on which the user has been created
   * Original Source: https://hugo.moe/discord/discord-id-creation-date.html
   *
   * @param id Discord snowflake for the user
   * @returns string
   */
  private static getCreationDate(id: string): string {
    // Turn our snowflake into a Date object
    const date = snowflakeToDate(id);

    // Apply  Discord timestamp formatting
    return dateMention(date, 'f');
  }
}
