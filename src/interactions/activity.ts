import { Logger, Random, Discord, InteractionResponseTypes, Configure } from '../../deps.ts';
import { Interaction } from '../interfaces/interaction.ts';

interface ActivityData {
  hours: string;
  activity: number;
}

interface Timezone {
  city: string;
  city_ascii: string;
  lat: number;
  lng: number;
  pop: number;
  country: string;
  iso2: string;
  iso3: string;
  province: string;
  timezone: string;
  state_ansi?: string;
}

export class ActivityInteraction implements Interaction {
  // TODO: Get this from Configure
  private static readonly url = 'http://druid.finlaydag33k.nl:8888/druid/v2/sql';
  private interaction: any;
  private static timezones: Timezone[]|null = null;

  public constructor(opts: any) {
    this.interaction = opts.interaction;
  }

  public async execute(): Promise<void> {
    await Discord.getBot().helpers.sendInteractionResponse(
      this.interaction.id,
      this.interaction.token,
      {
        type: InteractionResponseTypes.ChannelMessageWithSource,
        data: {
          embeds: [{
            color: 8263,
            title: `Das Propaganda Maschine Server Activity - Preparing`,
            description: `I'll be obtaining the latest graph on the server activity.\r\nPlease give me a moment to prepare it for you.`,
          }]
        }
      }
    );

    // Load timezone data
    try {
      if(ActivityInteraction.timezones === null) {
        const json = await Deno.readTextFile(`${Deno.cwd()}/src/extra/timezone-map.json`);
        ActivityInteraction.timezones = <Timezone[]>JSON.parse(json);
      }
    } catch(e) {
      // Log error
      Logger.error(`Could not load timezone data: "${e.message}"`, e.stack);

      // Update embed
      await this.sendError();
      return;
    }

    // Get the desired timezone (defaults to "Europe/Amsterdam")
    let timezone = 'Europe/Amsterdam';
    try {
      if(typeof this.interaction.data.options !== 'undefined') {
        const timezoneInput = this.interaction.data.options.find((option: any) => option.name === 'timezone');
        const match = ActivityInteraction.findTimezone(timezoneInput.value.toLowerCase());
        if(match) timezone = match.timezone;
      }
    } catch(e) {
      // Log error
      Logger.error(`Could not parse timezone: "${e.message}"`, e.stack);

      // Update embed
      await this.sendError();
      return;
    }

    // Obtain data from Druid
    let data;
    try {
      Logger.debug('Obtaining data from Druid...');
      data = await ActivityInteraction.query(timezone);
      if(data === null) throw Error('Data returned invalid');
    } catch(e) {
      // Log error
      Logger.error(`Could not load Druid data: "${e.message}"`, e.stack);

      // Update embed
      await this.sendError();
      return;
    }


    // Normalise our data
    let normalized;
    try {
      Logger.debug('Normalizing data...');
      normalized = ActivityInteraction.normalize(data);
    } catch(e) {
      // Log error
      Logger.error(`Could not normalize Druid data: "${e.message}"`, e.stack);

      // Update embed
      await this.sendError();
      return;
    }

    // Then create out graph
    let file;
    try {
      Logger.debug('Creating graph...');
      normalized.timezone = timezone;
      file = await ActivityInteraction.graph(normalized);
    } catch(e) {
      // Log error
      Logger.error(`Could not create graph: "${e.message}"`, e.stack);

      // Update embed
      await this.sendError();
      return;
    }

    // Read file into blob
    let fileBlob;
    try {
      const fileArray = await Deno.readFile(file);
      fileBlob = new Blob([fileArray.buffer], { type: 'image/png' });
    } catch(e) {
      // Log error
      Logger.error(`Could not create fileBlob: "${e.message}"`, e.stack);

      // Update embed
      await this.sendError();
      return;
    }

    // Reply with graph
    Logger.debug('Sending graph...');
    await Discord.getBot().helpers.editInteractionResponse(
      this.interaction.token,
      {
        file: {
          blob: fileBlob,
          name: 'activity.png',
        },
        embeds: [
          {
            color: 8263,
            title: `Das Propaganda Maschine Server Activity`,
            description: `Below the activity graph for the timezone "\`${timezone}\`".\r\nPlease note that this graph is for indication only and does not guarantee activity during the times shown.`,
          },
          {
            type: 'image',
            image: {url: 'attachment://activity.png'},
          }
        ]
      }
    );
  }

  private static async query(timezone: string): Promise<ActivityData[]|null> {
    let query;
    try {
      query = await Deno.readTextFile(`${Deno.cwd()}/src/extra/druid-activity.sql`);
      query = query.replace('{{TIMEZONE}}', timezone);
      query = query.replace('{{DATASET}}', Configure.get('druid_activity_dataset', 'yukikaze-activity'));
    } catch(e) {
      Logger.error(`Could not read Druid Activity query template: "${e.message}`, e.stack);
      return null;
    }

    let resp;
    try {
      resp = await fetch(ActivityInteraction.url, {
        method: 'POST',
        body: JSON.stringify({query: query}),
        headers: {
          'Content-Type': 'application/json'
        }
      });
    } catch(e) {
      Logger.error(`Could not obtain data from Druid: "${e.message}`, e.stack);
      return null;
    }

    if(resp.status !== 200) return null;

    try {
      return <ActivityData[]>await resp.json();
    } catch(e) {
      Logger.error(`Could not parse data from Druid: "${e.message}`, e.stack);
      return null;
    }
  }

  private static normalize(data: ActivityData[]) {
    // Split activity and hours
    let hours = [];
    let activity = [];
    let samples = 0;
    for(const entry of data) {
      hours.push(entry.hours);
      activity.push(entry.activity);
      samples += entry.activity;
    }

    // Remove "excessive" amounts
    const excessive = Math.min(...activity) - (Math.min(...activity) % 100);
    let normalized: number[] = [];
    for(const amount of activity) {
      normalized.push(amount - excessive);
    }

    // Turn into relative percentages and get bar colors
    const highest = Math.max(...normalized);
    let final: number[] = [];
    let colors: string[] = [];
    for(const amount of normalized) {
      const percentage = Math.trunc((amount / highest) * 100);
      final.push(percentage);
      colors.push(ActivityInteraction.barColor(percentage));
    }

    return {
      hours: hours,
      values: final,
      colors: colors,
      samples: samples,
      timezone: 'Europe/Amsterdam'
    };
  }

  private static barColor(percentage: number): string {
    if(percentage < 25) return 'red';
    if(percentage >= 25 && percentage < 50) return 'orange';
    return 'green';
  }

  private static async graph(data: any): Promise<string> {
    // Write our data to a random file
    const name = await Random.string(16);
    const getOSTempDir = () => Deno.env.get('TMPDIR') || Deno.env.get('TMP') || Deno.env.get('TEMP') || '/tmp';
    const path = `${getOSTempDir()}/druid-activity-${name}.json`;
    await Deno.writeFile(path, new TextEncoder().encode(JSON.stringify(data)));

    const p = Deno.run({cmd: ['python3', `${Deno.cwd()}/src/extra/druid-activity.py`, path]});
    await p.status();
    p.close();

    return path.replace('.json', '.png');
  }

  private static findTimezone(input: string): Timezone|null {
    if(ActivityInteraction.timezones === null) return null;
    for(const obj of ActivityInteraction.timezones) {
      const isMatch = Object.values(obj)
        .map(
          (entry: any) => {
            if(typeof entry === 'string') return entry.toLowerCase()
          })
        .includes(input)
      if (isMatch) return obj;
    }
    return null;
  }

  private async sendError(): Promise<void> {
    await Discord.getBot().helpers.editInteractionResponse(
      this.interaction.token,
      {
        embeds: [
          {
            color: 15158332,
            title: `Das Propaganda Maschine Server Activity - Error`,
            description: `It appears I have done something wrong while preparing the graph...\r\n<@91616138860978176>, mind having a look at my logs?`,
          }
        ]
      }
    );
  }
}
