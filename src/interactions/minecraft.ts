import {
  Logger,
  RCON,
  ApplicationCommandOptionTypes,
  InteractionResponseTypes,
  Configure,
  Discord,
  ApplicationCommandFlags
} from "../../deps.ts";
import { Interaction } from "../interfaces/interaction.ts";

export class MinecraftInteraction implements Interaction {
  private interaction: any;

  public constructor(opts: any) {
    this.interaction = opts.interaction;
  }

  /**
   * Main logic for this interaction:
   * - Delegate subcommand to the proper method
   *
   * @returns Promise<void>
   */
  public async execute(): Promise<void> {
    // Get the subcommand object
    const subcommand = this.interaction.data.options.find((option: any) => option.type === ApplicationCommandOptionTypes.SubCommand);

    // Handle the specific subcommand
    switch(subcommand.name) {
      case 'whitelist':
        await this.whitelist(subcommand.options);
        break;
      default:
        break;
    }
  }

  /**
   * Add a user to the whitelist over RCON
   *
   * @param opts
   * @returns Promise<void>
   */
  private async whitelist(opts: any): Promise<void> {
    // Get the username
    let username = opts.find((option: any) => option.name === 'username');
    if(!username) {
      Discord.getBot().helpers.sendInteractionResponse(
        this.interaction.id,
        this.interaction.token,
        {
          type: InteractionResponseTypes.ChannelMessageWithSource,
          data: {
            flags: ApplicationCommandFlags.Ephemeral,
            embeds: [
              {
                color: 15158332,
                title: 'Error',
                description: `I am unsure who you want me to whitelist...\r\nPlease specify a username.`
              }
            ]
          }
        }
      );
    }

    // Lowercase the username
    username = username.value.toLowerCase();

    // Whitelist user with RCON
    try {
      let rconHost = '';
      if(!Configure.get('minecraft_rcon_host')) {
        throw Error('minecraft_cron_host not set!');
      } else {
        rconHost = Configure.get('minecraft_rcon_host');
      }

      let rconPort = 25565;
      if(!Configure.get('minecraft_rcon_port')) {
        throw Error('minecraft_rcon_port not set!');
      } else {
        rconPort = parseInt(Configure.get('minecraft_rcon_host', '25565'));
      }

      let rconPass = '';
      if(!Configure.get('minecraft_rcon_pass')) {
        throw Error('MINECRAFT_RCON_PASS not set!');
      } else {
        rconPass = Configure.get('minecraft_rcon_pass', '');
      }

      // Connect to the RCON
      Logger.debug(`Connecting to RCON...`);
      const rconConn = new RCON();
      await rconConn.connect(rconHost, rconPort, rconPass);

      // Send the whitelist command
      Logger.debug(`Sending whitelist command`);
      const resp: string = await rconConn.send(`whitelist add ${username}`);
      Logger.debug(`RCON response: "${resp}"`);

      // Close the connection
      Logger.debug(`Closing connection...`);
      await rconConn.close();

      // Check if the user was added
      if(resp.toLowerCase() === `added ${username} to the whitelist`) {
        Logger.debug(`User "${username}" has been whitelisted!`);
        Discord.getBot().helpers.sendInteractionResponse(
          this.interaction.id,
          this.interaction.token,
          {
            type: InteractionResponseTypes.ChannelMessageWithSource,
            data: {
              embeds: [
                {
                  color: 8263,
                  title: 'Minecraft - Whitelist',
                  description: `I have added ${username} to the whitelist!\r\nYou can find all information needed to join below!\r\nFor the best experience, we recommend using the official DPM modpack but this is optional.\r\n\r\nEnjoy playing!`,
                  fields: [
                    {name: 'Address', value: `mc.finlaydag33k.nl`, inline: true},
                    {name: 'World Map', value: `https://mc.finlaydag33k.nl`, inline: true},
                    {name: 'DPM Modpack (optional)', value: `https://www.technicpack.net/modpack/das-propaganda-maschine.1859848`, inline: true}
                  ]
                }
              ]
            }
          }
        );
        return;
      }

      // Check if the user already was added prior
      if(resp === 'Player is already whitelisted') {
        Logger.debug(`User "${username}" was already whitelisted!`);
        Discord.getBot().helpers.sendInteractionResponse(
          this.interaction.id,
          this.interaction.token,
          {
            type: InteractionResponseTypes.ChannelMessageWithSource,
            data: {
              flags: ApplicationCommandFlags.Ephemeral,
              embeds: [
                {
                  color: 8263,
                  title: 'Minecraft - Whitelist',
                  description: `${username} was already whitelisted!\r\nYou can find all information needed to join below!\r\nFor the best experience, we recommend using the official DPM modpack but this is optional.\r\n\r\nEnjoy playing!`,
                  fields: [
                    {name: 'Address', value: `mc.finlaydag33k.nl`, inline: true},
                    {name: 'World Map', value: `https://mc.finlaydag33k.nl`, inline: true},
                    {name: 'DPM Modpack (optional)', value: `https://www.technicpack.net/modpack/das-propaganda-maschine.1859848`, inline: true}
                  ]
                }
              ]
            }
          }
        );
        return;
      }

      Discord.getBot().helpers.sendInteractionResponse(
        this.interaction.id,
        this.interaction.token,
        {
          type: InteractionResponseTypes.ChannelMessageWithSource,
          data: {
            flags: ApplicationCommandFlags.Ephemeral,
            embeds: [
              {
                color: 15158332,
                title: 'Error',
                description: `Something went wrong while trying to add ${username} to the whitelist.\r\nPlease try again later!`
              }
            ]
          }
        }
      );
    } catch(e) {
      Logger.error(`Could not add user to whitelist: "${e.message}"`, e.stack);
      Discord.getBot().helpers.sendInteractionResponse(
        this.interaction.id,
        this.interaction.token,
        {
          type: InteractionResponseTypes.ChannelMessageWithSource,
          data: {
            flags: ApplicationCommandFlags.Ephemeral,
            embeds: [
              {
                color: 15158332,
                title: 'Error',
                description: `Something went wrong while trying to add ${username} to the whitelist.\r\nPlease try again later!`
              }
            ]
          }
        }
      );
    }
  }
}
