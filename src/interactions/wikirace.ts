import {Logger, InteractionResponseTypes, Discord, ApplicationCommandFlags} from "../../deps.ts";
import { Interaction } from "../interfaces/interaction.ts";

export class WikiraceInteraction implements Interaction {
  private interaction: any;

  public constructor(opts: any) {
    this.interaction = opts.interaction;
  }

  /**
   * Main logic for this interaction:
   * - Obtain an origin article
   * - Obtain a destination article
   * - Send challenge to the user
   *
   * @returns Promise<void>
   */
  public async execute(): Promise<void> {
    // Inform the user we're working on it
    Logger.debug('Sending initial reply...');
    await Discord.getBot().helpers.sendInteractionResponse(
      this.interaction.id,
      this.interaction.token,
      {
        type: InteractionResponseTypes.ChannelMessageWithSource,
        data: {
          flags: ApplicationCommandFlags.Ephemeral,
          embeds: [{
            color: 8263,
            title: `Wikirace - Preparing`,
            description: `I'll be obtaining a Wikirace for you.\r\nPlease give me a moment to prepare it.`,
          }]
        }
      }
    );

    // Get origin and destination for the race
    Logger.debug('Getting Origin and dest...');
    const [origin, dest] = await Promise.all([WikiraceInteraction.getArticle(), WikiraceInteraction.getArticle()]);

    // Make sure both an origin and destination were found
    if(origin === '' || dest === '') {
      await Discord.getBot().helpers.editInteractionResponse(
        this.interaction.token,
        {
          type: InteractionResponseTypes.ChannelMessageWithSource,
          data: {
            flags: ApplicationCommandFlags.Ephemeral,
            embeds: [
              {
                color: 15158332,
                title: 'Error',
                description: `Something went wrong while trying to prepare a wikirace for you...\r\n Please try again later.`
              }
            ]
          }
        }
      );
      return;
    }

    // Create fields object
    const fields = [
      { name: 'Origin', value: `https://en.wikipedia.org/wiki/${origin}`, inline: true },
      { name: 'Destination', value: `https://en.wikipedia.org/wiki/${dest}`, inline: true },
      { name: 'Shortest Possible Path', value: 'Calculating...', inline: false }
    ];

    // Send the challenge to the user
    Logger.debug('Sending result!');
    try {
      await Discord.getBot().helpers.editInteractionResponse(
        this.interaction.token,
        {
          embeds: [
            {
              color: 8263,
              title: 'Wikirace',
              description: `Here is a randomly generated Wiki Race challenge!\r\nI wish you good luck!`,
              fields: fields
            }
          ]
        }
      );
    } catch(e) {
      Logger.error(`Could send Wikirace challenge: ${e.message}`, e.stack);
    }

    // Try to get the shortest route possible
    Logger.debug('Getting shortest route from SDOW');
    let shortest = 0;
    try {
      shortest = await WikiraceInteraction.getSolution(origin, dest);
      Logger.debug(`Found shortest route: ${shortest} articles!`);
    } catch(e) {
      Logger.error(`Could not get shortest path: "${e.message}"`, e.stack);
    }

    // Update embed with shortest route
    Logger.debug('Adding SRP...');
    fields[2].value = shortest === 0 ? 'Unknown' : `${shortest} Articles`;
    try {
      await Discord.getBot().helpers.editInteractionResponse(
        this.interaction.token,
        {
          embeds: [
            {
              color: 8263,
              title: 'Wikirace',
              description: `Here is a randomly generated Wiki Race challenge!\r\nI wish you good luck!`,
              fields: fields
            }
          ]
        }
      );
    } catch(e) {
      Logger.error(`Could update Wikirace challenge: ${e.message}`, e.stack);
    }
  }

  /**
   * Get a random article from Wikipedia's API.
   *
   * @returns Promise<string>
   */
  private static async getArticle(): Promise<string> {
    // Send a request
    let resp;
    try {
      resp = await fetch('https://en.wikipedia.org/api/rest_v1/page/random/title', {
        headers: {
          Accept: 'application/json',
          'User-Agent': 'Yukikaze (https://gitlab.com/finlaydag33k/yukikaze; contact@finlaydag33k.nl)'
        }
      }).then(resp => resp.json());
    } catch(e) {
      Logger.error(`Could not get article: "${e.message}"`, e.stack);
      return '';
    }

    // Get the title
    if(resp.items[0].hasOwnProperty('title')) return resp.items[0].title;

    // No title was found
    Logger.error(`Could not get article! (No title)`);
    return '';
  }

  /**
   * Get a solution from the SDOW API
   *
   * @params origin Origin article to start from
   * @params dest Destination artible to end at
   * @returns Promise<string>
   */
  private static async getSolution(origin: string, dest: string): Promise<number> {
    // Get the article names from the url
    origin = origin
      .replace('https://en.wikipedia.org/wiki', '')
      .replace('_', ' ');
    dest = dest
      .replace('https://en.wikipedia.org/wiki', '')
      .replace('_', ' ');

    // Make request to API
    let resp;
    try {
      resp = await fetch('https://api.sixdegreesofwikipedia.com/paths', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'User-Agent': 'Yukikaze (https://gitlab.com/finlaydag33k/yukikaze; contact@finlaydag33k.nl)'
        },
        body: JSON.stringify({
          source: origin,
          target: dest
        }),
      });
      resp = await resp.json();
    } catch(e) {
      Logger.error(`Could not get shortest route: "${e.message}`, e.stack);
      return 0;
    }

    // Get the shortest path
    const shortest = resp.paths.reduce((prev: any, next: any) => prev.length > next.length ? next : prev);

    // Return length of the shortest path
    return shortest.length;
  }
}
