import { Scores, Schema as ScoreEntry } from "./schema/scores.ts";

export class Leaderboard {
  public static readonly exclusions = [
    "91616138860978176", // Shimakaze#9137
    // TODO: Automatically exclude bots as-is
    "937731388407439462", // Yukikaze#2629
    "432610292342587392", // Mudae#0807
    "692045914436796436", // Truth or Dare#8712
    "812323565012647997", // WaifuGame#1043
    "646937666251915264", // Karuta#1280
    "302050872383242240", // Disboard#2760
  ];
  protected static board: ScoreEntry[] = [];
  protected static last_updated: Date|null = null;
  public static get lastUpdated(): Date|null { return Leaderboard.last_updated; }

  public static async update() {
    Leaderboard.board = await Scores().find({
      snowflake: { $nin: Leaderboard.exclusions }
    }, { sort: { total_score: -1 } }) as ScoreEntry[];
    Leaderboard.last_updated = new Date();
  }

  public static top10() {
    return Leaderboard.board.slice(0, 10);
  }

  public static find(id: string) {
    const index = Leaderboard.board.findIndex((entry: ScoreEntry) => entry.snowflake === id);
    return {
      id: id,
      position: index + 1,
      data: Leaderboard.board[index] as ScoreEntry,
    };
  }
}
