import { Time } from "../deps.ts";
import { ActivityQueue, Schema as IActivity } from "./schema/activity-queue.ts";

export class Activity {
  /**
   * Get a chunk from the buffer
   *
   * @param size Amount of items to get from the queue
   * @param remove Whether to delete the obtained items from the queue
   */
  public static async getChunk(size: number = 100, remove: boolean = false): Promise<IActivity[]> {
    let buf: IActivity[] = [];

    while(buf.length < size) {
      // Run findOne/findOneAndRemove on whether we need to remove it or not
      let entry;
      if(remove) {
        entry = await ActivityQueue().findOneAndRemove();
      } else {
        entry = await ActivityQueue().findOne();
      }

      // Make sure an entry was found
      if(!entry) break;

      // Add entry to the buffer
      buf.push(entry);
    }

    return buf;
  }

  /**
   * Add activity to the queue
   *
   * @param user
   * @param channel
   * @param time
   */
  public static async add(user: BigInt, channel: BigInt, time: Time|Date = new Time()): Promise<void> {
    let timeString = '';
    if(time instanceof Time) {
      timeString = time.getTime.toISOString();
    } else {
      timeString = time.toISOString();
    }

    await ActivityQueue().insertOne({
      time: timeString,
      user: user.toString(),
      channel: channel.toString(),
    });
  }
}
