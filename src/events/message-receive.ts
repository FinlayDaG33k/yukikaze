import { Logger, Configure } from "../../deps.ts";
import { ScanMessages } from "../util/scan-messages.ts";
import { Event } from "../interfaces/event.ts";
import { Activity } from "../activity.ts";
import { getChannel } from "../util/get-channel.ts";

export class MessageReceiveEvent implements Event {
  /**
   * Main logic for this event:
   * - Updates the message count for the user
   * - Updates the last seen for the user
   * - Updates the scan checkpoint for the user
   *
   * @param opts
   * @returns Promise<void>
   */
  public async execute(opts: any): Promise<void> {
    // Check if the channel has finished scanning
    // If so, start tracking
    if(ScanMessages.isFinished(opts.message.channelId)) {
      await Promise.all([
        ScanMessages.increaseChannelCount(opts.message.authorId, opts.message.channelId),
        ScanMessages.increaseMemberCount(opts.message.authorId, opts.message.timestamp),
        ScanMessages.updateCheckpoint(opts.message.channelId, opts.message.id),
        MessageReceiveEvent.logLoki(opts.message, opts.message.timestamp),
        Activity.add(opts.message.authorId, opts.message.channelId, new Date(opts.message.timestamp)),
      ]);
    }
  }

  /**
   * Add the message event to the Loki database
   *
   * @param message
   * @param timestamp
   * @returns Promise<void>
   */
  private static async logLoki(message: any, timestamp: number): Promise<void> {
    // Ignore bots
    if(message.isBot) return;

    // Get channel
    const channel = await getChannel(message.channelId);
    if(!channel) {
      Logger.error(`Could not add message to Loki: Channel not found! (this is a bug!)`);
      return;
    }

    // Build our log data
    const data = {
      streams: [
        {
          stream: { application: Configure.get('loki_application', 'yukikaze'), event: 'message' },
          values: [
            [(timestamp * 1_000_000).toString(), `${message.tag} posted in #${channel.name}`]
          ]
        }
      ]
    }

    try {
      const resp = await fetch(`${Configure.get('loki_host', 'http://loki')}/loki/api/v1/push`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'X-Scope-OrgID': 'FinlayDaG33k-Yukikaze-GaLXzJZTpd2wQFJx'
        },
        body: JSON.stringify(data)
      });
      if(resp.ok === false) throw Error(`Response non-OK: ${resp.statusText}`)
    } catch(e) {
      Logger.error(`Could not add message to Loki: ${e.message}`, e.stack);
    }
  }
}
