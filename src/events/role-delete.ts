import { Event } from "../interfaces/event.ts";
import { RolesCache } from "../util/roles-cache.ts";
import { findChannelByName } from "../util/find-channel-by-name.ts";
import { Logger, Discord } from "../../deps.ts";

export class RoleDeleteEvent implements Event {
  /**
   * Main logic for this event:
   * - Unknown
   *
   * @param opts
   * @returns Promise<void>
   */
  public async execute(opts: any): Promise<void> {
    Logger.info(`Detected deletion of role!`);

    // Find the role (so we can keep the name)
    const role = RolesCache.get(opts.role.roleId);
    if(!role) {
      Logger.error(`Could not find role "${opts.role.roleId}" in cache! (this is a bug!)`);
      return;
    }

    // Remove the role from cache
    RolesCache.remove(opts.role.roleId);

    // Find our logs channel
    const channel = await findChannelByName('logs');
    if(!channel) return;

    // Send message to inform staff
    try {
      await Discord.getBot().helpers.sendMessage(
        channel.id,
        {
          embeds: [{
            color: 15105570,
            title: `Das Propaganda Maschinee Server Audit - Role Deleted`,
            description: `Role "\`${role.name}\`" was deleted.\r\nPlease check if this was done in accordance to the current MIP specifications.`,
          }]
        }
      );
    }catch(e) {
      Logger.error(`Could not send audit message: "${e.message}"`, e.stack);
    }
  }
}
