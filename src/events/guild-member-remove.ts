import { Event } from "../interfaces/event.ts";
import { Logger, Configure } from "../../deps.ts";

export class GuildMemberRemoveEvent implements Event {
  /**
   * Main logic for this event:
   * - Log into Loki
   *
   * @param opts
   * @returns Promise<void>
   */
  public async execute(opts: any): Promise<void> {
    // Log leave event in Loki
    await GuildMemberRemoveEvent.logLoki(`${opts.username}#${opts.discriminator}`, new Date().getTime());
  }


  /**
   * Add the leave event to the Loki database
   *
   * @param username
   * @param timestamp
   * @returns Promise<void>
   */
  private static async logLoki(username: string, timestamp: number): Promise<void> {
    // Build our log data
    const data = {
      streams: [
        {
          stream: { application: Configure.get('loki_application', 'yukikaze'), event: 'join-leave' },
          values: [
            [(timestamp * 1_000_000).toString(), `${username} left Das Propaganda Maschine.`]
          ]
        }
      ]
    }

    try {
      const resp = await fetch(`${Configure.get('loki_host', 'http://loki')}/loki/api/v1/push`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'X-Scope-OrgID': 'FinlayDaG33k-Yukikaze-GaLXzJZTpd2wQFJx'
        },
        body: JSON.stringify(data)
      });
      if(resp.ok === false) throw Error(`Response non-OK: ${resp.statusText}`)
    } catch(e) {
      Logger.error(`Could not add message to Loki: ${e.message}`, e.stack);
    }
  }
}
