import { Event } from "../interfaces/event.ts";
import { RolesCache } from "../util/roles-cache.ts";
import { Logger, Discord } from "../../deps.ts";
import { findChannelByName } from "../util/find-channel-by-name.ts";

export class RoleUpdateEvent implements Event {
  /**
   * Main logic for this event:
   * - Unknown
   *
   * @param opts
   * @returns Promise<void>
   */
  public async execute(opts: any): Promise<void> {
    // Find role from the cache
    const cached = RolesCache.get(opts.role.id);
    if(!cached) {
      Logger.error(`Could not find role "${opts.role.roleId}" in cache! (this is a bug!)`);
      return;
    }

    // Check for any differences
    const differences = this.diff(cached, opts.role);
    if(differences.length === 0) return;

    // Add the differences to fields
    const fields = [];
    if(differences.includes('name')) {
      fields.push({ name: 'Previous Name', value: `\`${cached.name}\``, inline: true });
      fields.push({ name: 'Current Name', value:  `\`${opts.role.name}\``, inline: true });
    } else {
      fields.push({ name: 'Role Name', value: `\`${opts.role.name}\``});
    }
    fields.push({ name: 'Updated Permissions', value: `\`${differences.includes('permissions') ? 'Yes' : 'No'}\``, inline: true });

    // Update our cache
    RolesCache.set(opts.role.id, opts.role);

    // Find our logs channel
    const channel = await findChannelByName('logs');
    if(!channel) return;

    // Send message to inform staff
    try {
      await Discord.getBot().helpers.sendMessage(
        channel.id,
        {
          embeds: [{
            color: 15105570,
            title: `Das Propaganda Maschine Server Audit - Role Updated`,
            description: `A Role was updated.\r\nPlease check if this was done in accordance to the current MIP specifications.`,
            fields: fields
          }]
        }
      );
    }catch(e) {
      Logger.error(`Could not send audit message: "${e.message}"`, e.stack);
    }
  }

  public diff(old: any, current: any) {
    const differences: string[] = [];
    if(old.name !== current.name) differences.push('name');
    if(old.permissions !== current.permissions) differences.push('permissions');

    return differences;
  }
}
