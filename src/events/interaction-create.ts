import { Logger, InteractionResponseTypes, Discord, InteractionDispatcher } from "../../deps.ts";
import { Event } from "../interfaces/event.ts";

export class InteractionCreateEvent implements Event {
  /**
   * Main logic for this event:
   * - Delegates interactions (slashcommands) to the appropriate handler
   *
   * @param opts
   * @returns Promise<void>
   */
  public async execute(opts: any): Promise<void> {
    // Get the interaction name
    const interactionName = opts.data.name;

    // Load the interaction
    Logger.info(`Running interaction "${interactionName}" for "${opts.user.username}#${opts.user.discriminator}" (interaction id: ${opts.id})`);
    try {
      await InteractionDispatcher.dispatch(interactionName, {interaction: opts});
      Logger.info(`Finished running interaction "${interactionName}" for "${opts.user.username}#${opts.user.discriminator}" (interaction id: ${opts.id})`);
    } catch(e) {
      Logger.error(`Failed running interaction "${interactionName}" for "${opts.user.username}#${opts.user.discriminator}" (interaction id: ${opts.id}): ${e.message}`, e.stack);
      Discord.getBot().helpers.sendInteractionResponse(
        opts.id,
        opts.token,
        {
          type: InteractionResponseTypes.ChannelMessageWithSource,
          data: {
            embeds: [
              {
                color: 15158332,
                title: 'Error',
                description: `Something went wrong while trying to execute the command, please try again later!`
              }
            ]
          }
        }
      );
    }
  }
}
