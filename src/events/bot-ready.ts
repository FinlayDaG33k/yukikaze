import { Logger, Configure, InteractionDispatcher } from "../../deps.ts";
import { ScanMessages } from "../util/scan-messages.ts";
import { Event } from "../interfaces/event.ts";
import { Leaderboard } from "../leaderboard.ts";

export class BotReadyEvent implements Event {
  /**
   * Main logic for this event:
   * - Syncs supported interactions with the Discord Gateway
   * - Initiates a message scan for missed messages while offline
   *
   * @returns Promise<void>
   */
  public async execute(): Promise<void> {
    Logger.info('Bot has connected!');

    // Upsert interactions to Discord
    Logger.info(`Updating interactions to Discord...`);
    await InteractionDispatcher.update({
      guildId: BigInt(Configure.get('discord_guild', 0))
    })
    Logger.info(`Finished updating interactions!`);

    // Update leaderboard (yes really, before re-scanning)
    await Leaderboard.update();

    // Scan previous messages we may have missed
    await ScanMessages.scan();
  }
}
