import { Discord, Logger } from "../../deps.ts";
import { Event } from "../interfaces/event.ts";
import { RolesCache } from "../util/roles-cache.ts";
import { findChannelByName } from "../util/find-channel-by-name.ts";

export class RoleCreateEvent implements Event {
  /**
   * Main logic for this event:
   * - Add the role to our cache.
   * - Inform staff of the creation.
   *
   * @param opts
   * @returns Promise<void>
   */
  public async execute(opts: any): Promise<void> {
    // Add role to the cache
    Logger.info(`Detected creation of new role!`);
    RolesCache.set(opts.role.id, opts.role);

    // Find our logs channel
    const channel = await findChannelByName('logs');
    if(!channel) return;

    // Send message to inform staff
    try {
      await Discord.getBot().helpers.sendMessage(
        channel.id,
        {
          embeds: [{
            color: 15105570,
            title: `Das Propaganda Maschine Server Audit - Role Created`,
            description: `Role "\`${opts.role.name}\`" was created.\r\nPlease check if this was done in accordance to the current MIP specifications.`,
          }]
        }
      );
    }catch(e) {
      Logger.error(`Could not send audit message: "${e.message}"`, e.stack);
    }
  }
}
