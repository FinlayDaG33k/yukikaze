import { Features } from "../features.ts";
import { Event } from "../interfaces/event.ts";
import { checkChannel } from "../util/check-channel.ts";

export class MessageReactionAddEvent implements Event {
  /**
   * Main logic for this event:
   * - Emits an event for self role assignment (if reaction happens in "self-roles" channel)
   *
   * @param opts
   * @returns Promise<void>
   */
  public async execute(opts: any): Promise<void> {
    // Check if we're in the self-roles channel
    if(await checkChannel({guild: opts.guildId, channel: opts.channelId}, 'self-roles')) {
      await Features.dispatch('SelfRoleAssign', {
        user: opts.member,
        emoji: opts.emoji.name
      });
      return;
    }
  }
}
