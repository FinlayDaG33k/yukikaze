import { Logger } from "../../deps.ts";
import { Event } from "../interfaces/event.ts";
import { RolesCache } from "../util/roles-cache.ts";

export class GuildLoadedEvent implements Event {
  /**
   * Main logic for this event:
   * - Cache list of all roles for the guild
   *
   * @param opts
   * @returns Promise<void>
   */
  public async execute(opts: any): Promise<void> {
    Logger.debug(`Populating roles cache...`);
    for(const role of opts.roles) {
      RolesCache.set(role[0], role[1]);
    }
    Logger.debug(`Finished populating roles cache!`);
  }
}
