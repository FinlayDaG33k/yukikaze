import { VoiceLog } from "../schema/voice-log.ts";

export class VoiceStateUpdateEvent implements Event {
  /**
   * Main logic for this event:
   * - Syncs supported interactions with the Discord Gateway
   * - Initiates a message scan for missed messages while offline
   *
   * @returns Promise<void>
   */
  public async execute(opts: any): Promise<void> {
    typeof opts.channelId !== 'undefined' ? await this.logJoin(opts.userId) : await this.logLeave(opts.userId);
  }

  public async logJoin(user: BigInt): Promise<void> {
    await VoiceLog().insertOne({
      user: user.toString(),
      timestamp: new Date(),
      action: true,
    });
  }

  public async logLeave(user: BigInt): Promise<void> {
    await VoiceLog().insertOne({
      user: user.toString(),
      timestamp: new Date(),
      action: false,
    });
  }
}
