import { Logger, Discord, Configure } from "../../deps.ts";
import { Event } from "../interfaces/event.ts";

export class GuildMemberAddEvent implements Event {
  /**
   * Main logic for this event:
   * - Assigns the default "Ensign" role to new members
   * - Sends the user a DM to welcome them
   *
   * @param opts
   * @returns Promise<void>
   */
  public async execute(opts: any): Promise<void> {
    Logger.info(`User "${opts.user.username}#${opts.user.discriminator}" has joined the server!`);
    // Add ensign role
    // Send welcome message
    // Log join event to Loki
    await Promise.all([
      this.assignEnsign(opts),
      this.sendWelcome(opts),
      GuildMemberAddEvent.logLoki(`${opts.user.username}#${opts.user.discriminator}`, opts.member.joinedAt)
    ]);
  }

  /**
   * Add the default role of "Ensign" to new members
   *
   * @param opts
   * @returns Promise<void>
   */
  public async assignEnsign(opts: any): Promise<void> {
    // Get the guild information
    const guild = await Discord.getBot().helpers.getGuild(opts.member.guildId);
    if(!guild) {
      Logger.error(`Guild could not be found! (this is definitely a bug!)`);
      return;
    }

    // Get the Ensign role
    const role = guild.roles.find((role: any) => role.name === 'Ensign');
    if(!guild) {
      Logger.error(`Role "Ensign" could not be found! (Did you set it up?)`);
      return;
    }

    // Assign the role
    try {
      await Discord.getBot().helpers.addRole(opts.member.guildId, opts.member.id, role.id, 'Server join');
    } catch(e) {
      Logger.error(`Could not assign role "Ensign" to user "${opts.user.username}#${opts.user.discriminator}": ${e.message}`, e.stack);
    }
  }

  /**
   * Send the user a welcome message in their DM
   *
   * @param opts
   * @returns Promise<void>
   */
  public async sendWelcome(opts: any): Promise<void> {
    // Get the user's DM channel
    const channel = await Discord.getBot().helpers.getDmChannel(opts.user.id);
    if(!channel) {
      Logger.error(`DM channel could not be found! (this is likely a bug!)`);
      return;
    }

    // Create an embed
    const embed = {
      color: 8263,
      title: 'Welcome to Das Propaganda Maschine',
      description: `Hello ${opts.user.username},
      My name is Yukikaze and on behalf of Teitoku, I want to welcome you to Das Propaganda Maschine.
      I have been created by Teitoku specifically for DPM to help people with all kinds of stuff.
      If you want to see what I can do, please visit my [wiki](https://yukikaze.finlaydag33k.nl).
      Feel free to call me at any time and I'll help you to the best of my ability, but I'm still very young and learning, so please don't be angry at me if I make a mistake.
  
      Before you get started on DPM, you may want to check the "<#940696172065062972>" channel to unlock some additional channels.
      Please, don't feel shy to say anything on the server, we'd love to have the server bloom!

      I hope you will enjoy my service and your stay in Das Propaganda Maschine!
      `
    }

    // Send a DM
    try {
      await Discord.getBot().helpers.sendMessage(channel.id, {
        embeds: [embed]
      });
    } catch(e) {
      Logger.error(`Could send welcome to "${opts.user.username}#${opts.user.discriminator}": ${e.message}`, e.stack);
    }
  }

  /**
   * Add the join event to the Loki database
   *
   * @param username
   * @param timestamp
   * @returns Promise<void>
   */
  private static async logLoki(username: string, timestamp: number): Promise<void> {
    // Build our log data
    const data = {
      streams: [
        {
          stream: { application: Configure.get('loki_application', 'yukikaze'), event: 'join-leave' },
          values: [
            [(timestamp * 1_000_000).toString(), `${username} joined Das Propaganda Maschine.`]
          ]
        }
      ]
    }

    try {
      const resp = await fetch(`${Configure.get('loki_host', 'http://loki')}/loki/api/v1/push`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'X-Scope-OrgID': 'FinlayDaG33k-Yukikaze-GaLXzJZTpd2wQFJx'
        },
        body: JSON.stringify(data)
      });
      if(resp.ok === false) throw Error(`Response non-OK: ${resp.statusText}`)
    } catch(e) {
      Logger.error(`Could not add message to Loki: ${e.message}`, e.stack);
    }
  }
}
