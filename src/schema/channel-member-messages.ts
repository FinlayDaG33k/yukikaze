import { dango } from "../../deps.ts";

export interface Schema {
  _id: any; // Actually an ObjectId
  channel: string;
  member: string;
  count: number;
}

const schema = dango.schema({
  channel: 'string',
  member: 'string',
  count: 'number',
});

export const ChannelMemberMessages = () => dango.model('ChannelMemberMessages', schema);
