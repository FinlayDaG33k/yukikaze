import { dango } from "../../deps.ts";

export interface Schema {
  _id: any; // Actually an ObjectId
  snowflake: string;
  messages: number;
  joined_at: Date;
  last_seen: Date;
}

const schema = dango.schema({
  snowflake: 'string',
  messages: 'number',
  joined_at: 'date',
  last_seen: 'date',
});

export const GuildMember = () => dango.model('GuildMember', schema);
