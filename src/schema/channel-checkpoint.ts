import { dango } from "../../deps.ts";

export interface Schema {
  _id: any; // Actually an ObjectId
  channel: string;
  checkpoint: number;
}

const schema = dango.schema({
  channel: 'string',
  checkpoint: 'string',
});

export const ChannelCheckpoint = () => dango.model('ChannelCheckpoint', schema);
