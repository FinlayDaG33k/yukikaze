import { dango } from "../../deps.ts";

export interface Schema {
  _id: any; // Actually an ObjectId
  snowflake: string;
  messages: number;
  membership_days: number;
  reputation: number;
  total_score: number;
}

const schema = dango.schema({
  snowflake: 'string',
  messages: 'number',
  membership_days: 'number',
  reputation: 'number',
  total_score: 'number',
});

export const Scores = () => dango.model('Scores', schema);
