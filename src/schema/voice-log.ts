import { dango } from "../../deps.ts";

export interface Schema {
  _id: any; // Actually an ObjectId
  user: string;
  timestamp: Date;
  action: boolean;
}

const schema = dango.schema({
  user: 'string',
  timestamp: 'date',
  action: 'boolean',
});

export const VoiceLog = () => dango.model('VoiceLog', schema);
