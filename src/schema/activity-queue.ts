import { dango } from "../../deps.ts";

export interface Schema {
  _id: any; // Actually an ObjectId
  time: string;
  user: string;
  channel: string;
}

const schema = dango.schema({
  time: 'string',
  user: 'string',
  channel: 'string',
});

export const ActivityQueue = () => dango.model('ActivityQueue', schema);
