import { Logger, Discord } from "../../deps.ts";
import { Feature } from "../interfaces/feature.ts";
import { RoleList } from "../interfaces/role-list.ts";

export class SelfRoleAssignFeature implements Feature {
  private readonly user: any;
  private readonly emoji: any;

  // TODO: Add an easier way of syncing with SelfRoleRemoveFeature
  private readonly roles: RoleList[] = [
    { name: 'VOC', emoji: '🇳🇱' },
    { name: 'Shipfuckery', emoji: '🚢' },
    { name: 'Games', emoji: '🎮' },
    { name: 'Security & Privacy', emoji: '🔐' },
    { name: 'Tech', emoji: '🖥️' },
    { name: 'Weebery', emoji: '🇯🇵' },
    { name: 'Lewd City Member', emoji: '🛐' },
    { name: 'Femboy Enjoyer', emoji: '🍆' },
    { name: 'Engine go BRRRR', emoji: '🏎️'},
  ];

  public constructor(opts: any) {
    this.user = opts.user;
    this.emoji = opts.emoji;
  }

  /**
   * Handle assignments of a role
   *
   * @returns Promise<void>
   */
  public async execute(): Promise<void> {
    // Get the guild information
    const guild = await Discord.getBot().helpers.getGuild(this.user.guildId);
    if(!guild) {
      Logger.error(`Guild could not be found! (this is definitely a bug!)`);
      return;
    }

    // Get the role corresponding to the emoji
    const targetRole = this.roles.find((role: any) => role.emoji === this.emoji);
    if(!targetRole) {
      Logger.warning(`No role could be found for emoji "${this.emoji}".`);
      return;
    }

    // Get guild role for this emoji
    const role = guild.roles.find((role: any) => role.name === targetRole.name)
    if(!targetRole) {
      Logger.warning(`No role could be found for emoji "${this.emoji}" in guild. (did you set it up)`);
      return;
    }

    // Assign the role
    try {
      await Discord.getBot().helpers.addRole(this.user.guildId, this.user.id, role.id, 'Self-assign');
    } catch(e) {
      Logger.error(`Could not assign role "${targetRole.name}" to user "${this.user.username}#${this.user.discriminator}": ${e.message}`, e.stack);
    }

    // Get the user's DM channel
    const channel = await Discord.getBot().helpers.getDmChannel(this.user.id);
    if(!channel) {
      Logger.error(`DM channel could not be found! (this is likely a bug!)`);
      return;
    }

    // Send a DM
    try {
      await Discord.getBot().helpers.sendMessage(channel.id, {
        content: `You have been assigned the role "${targetRole.name}" in Das Propaganda Maschine!`
      });
    } catch(e) {
      Logger.error(`Could send notification to "${this.user.username}#${this.user.discriminator}": ${e.message}`, e.stack);
    }
  }
}
