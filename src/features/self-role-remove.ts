import { Logger, Discord } from "../../deps.ts";
import { RoleList } from "../interfaces/role-list.ts";
import { Feature } from "../interfaces/feature.ts";

export class SelfRoleRemoveFeature implements Feature {
  private readonly userId: any;
  private readonly guildId: any;
  private readonly emoji: any;

  // TODO: Add an easier way of syncing with SelfRoleRemoveFeature
  private readonly roles: RoleList[] = [
    { name: 'VOC', emoji: '🇳🇱' },
    { name: 'Shipfuckery', emoji: '🚢' },
    { name: 'Games', emoji: '🎮' },
    { name: 'Security & Privacy', emoji: '🔐' },
    { name: 'Tech', emoji: '🖥️' },
    { name: 'Weebery', emoji: '🇯🇵' },
    { name: 'Lewd City Member', emoji: '🛐' },
    { name: 'Femboy Enjoyer', emoji: '🍆' },
    { name: 'Engine go BRRRR', emoji: '🏎️'},
  ];

  public constructor(opts: any) {
    this.userId = opts.userId;
    this.guildId = opts.guildId;
    this.emoji = opts.emoji;
  }

  /**
   * Handle removing of a role
   *
   * @returns Promise<void>
   */
  public async execute() {
    // Get the guild information
    const guild = await Discord.getBot().helpers.getGuild(this.guildId);
    if(!guild) {
      Logger.error(`Guild could not be found! (this is definitely a bug!)`);
      return;
    }

    // Get the user information
    const user = await Discord.getBot().helpers.getMember(this.guildId, this.userId);
    if(!user) {
      Logger.error(`Guild could not be found! (this is most likely a bug!)`);
      return;
    }

    // Get the role corresponding to the emoji
    const targetRole = this.roles.find((role: any) => role.emoji === this.emoji);
    if(!targetRole) {
      Logger.warning(`No role could be found for emoji "${this.emoji}".`);
      return;
    }

    // Get guild role for this emoji
    const role = guild.roles.find((role: any) => role.name === targetRole.name)
    if(!targetRole) {
      Logger.warning(`No role could be found for emoji "${this.emoji}" in guild. (did you set it up)`);
      return;
    }

    // Assign the role
    try {
      await Discord.getBot().helpers.removeRole(this.guildId, this.userId, role.id, 'Self-assign');
    } catch(e) {
      Logger.error(`Could not remove role "${targetRole.name}" from user "${user.username}#${user.discriminator}": ${e.message}`, e.stack);
    }

    // Get the user's DM channel
    const channel = await Discord.getBot().helpers.getDmChannel(user.id);
    if(!channel) {
      Logger.error(`DM channel could not be found! (this is likely a bug!)`);
      return;
    }

    // Send a DM
    try {
      await Discord.getBot().helpers.sendMessage(channel.id, {
        content: `You have removed the role "${targetRole.name}" in Das Propaganda Maschine!`
      });
    } catch(e) {
      Logger.error(`Could send notification to "${user.username}#${user.discriminator}": ${e.message}`, e.stack);
    }
  }
}
