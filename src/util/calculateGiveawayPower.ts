/**
 * Calculate someone's giveaway power by means of their score
 * Just run it through a log function and truncate the decimals
 * Always returns a minimum of 1
 *
 * @param score
 * @returns number
 */
export function calculateGiveawayPower(score: number): number {
  // Run the score through a log
  let power = Math.log(score);

  // Truncate the decimals
  power = Math.trunc(power)

  // Check if we have atleast 1
  // If not, return 1
  // else return the real power
  if(power < 1) return 1;
  return power;
}
