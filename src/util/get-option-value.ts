export function getOptionValue(name: string, options: any) {
  // Make sure options are defined
  if(typeof options === 'undefined') return undefined;

  // Find the option by name
  const option = options.find((option: any) => option.name === name);

  // Return the value or undefined
  if(option) return option.value;
  return undefined;
}
