import { Logger, Discord } from "../../deps.ts";

/**
 * Check whether the passed channel has the specified channel name
 *
 * @param opts
 * @param channelName
 * @returns Promise<boolean>
 */
export async function checkChannel(opts: any, channelName: string): Promise<boolean> {
  const channel = await Discord.getBot().helpers.getChannel(opts.channel);
  if(!channel) {
    Logger.error(`Channel could not be found! (this is definitely a bug!)`);
    return false;
  }

  return channel.name === channelName;
}
