export function getMembershipDays(joined: number|string|Date, last_seen: number|string|Date): number {
  // Make sure the joined and last seen are instances of Date
  if(!(joined instanceof Date)) joined = new Date(joined);
  if(!(last_seen instanceof Date)) last_seen = new Date(last_seen);

  // Make sure the joined isn't "0" (not a member)
  if(joined.getTime() === 0) return 0;

  // Calculate and return the days since joining
  return Math.trunc((last_seen.getTime() - joined.getTime()) / (1000 * 3600 * 24));
}
