export class RolesCache {
  private static roles: any = {};

  public static set(id: any, role: any) {
    RolesCache.roles[id] = role;
  }

  public static get(id: any): any {
    if(!Object.hasOwn(RolesCache.roles, id)) return null;
    return RolesCache.roles[id];
  }

  public static findByName(name: string): any {
    return Object.values(RolesCache.roles).find((role: any) => role.name === name);
  }

  public static remove(id: any): void {
    if(!Object.hasOwn(RolesCache.roles, id)) return;
    delete RolesCache.roles[id];
  }
}
