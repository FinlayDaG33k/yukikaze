export function dateMention(date: Date = new Date(), flag: string = ''): string {
  // Turn date into UNIX timestamp (seconds)
  const timestamp = (date.getTime() / 1000) | 0;

  // Add flag if desired
  if(flag.length === 0) return `<t:${timestamp}>`;
  return `<t:${timestamp}:${flag}>`;
}
