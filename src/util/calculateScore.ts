/**
 * Calculate the score of a given the reputation, messages and time in the server
 * - 1 point per 5 messages (rewards activity over time)
 * - 1 point per 5 messages/day (rewards consistent activity)
 * - 1 point per rep (rewards positive behaviour)
 * - 1 point per 7 days in the server (rewards older members)
 *
 * @param rep
 * @param messages
 * @param membershipDays
 * @returns number
 */
export function calculateScore(rep: number, messages: number, membershipDays: number): number {
  // Set our score to 0
  let score = 0;

  // Divide our messages by 5
  // Add this to the score
  score += messages / 5;

  // Calculate average messages/day
  // Divide by 5 (truncate decimals)
  // Add this to the score
  if(membershipDays > 0 && messages > 5) score += Math.trunc((messages / membershipDays) / 5);

  // Divide our rep by 2
  // Add this to the score
  score += rep;

  // Get the days in the server
  // Add this to the score
  score += membershipDays / 7;

  // Truncate the decimals
  score = Math.trunc(score);

  // Return our final score
  return score;
}
