import { Discord, Configure, Logger } from "../../deps.ts";

export async function findUserById(id: BigInt) {
  try {
    const user = await Discord.getBot().helpers.getMember(BigInt(Configure.get('discord_guild', 0)), id);
    if(!user) return null;
    return user;
  } catch(_e) {
    Logger.debug(`User "${id}" is no longer a member of this guild!`);
    return null;
  }
}
