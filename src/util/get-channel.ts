import { Discord } from "../../deps.ts";

/**
 * Get a channel by its ID
 * TODO: Find out type of channel
 *
 * @param id
 */
export async function getChannel(id: any): Promise<any|null> {
  const channel = await Discord.getBot().helpers.getChannel(id);
  if(!channel) return null;
  return channel;
}
