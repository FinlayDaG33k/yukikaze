import { Logger, Configure, Discord } from "../../deps.ts";
import { Activity } from "../activity.ts";
import { findUserById } from "./find-user-by-id.ts";
import { ChannelCheckpoint, Schema as ChannelCheckpointEntry } from "../schema/channel-checkpoint.ts";
import { GuildMember, Schema as GuildMemberEntry } from "../schema/guild-member.ts";
import { ChannelMemberMessages, Schema as ChannelMemberMessagesEntry } from "../schema/channel-member-messages.ts";

interface Checkpoints {
  channel: BigInt;
  checkpoint: BigInt;
}

export class ScanMessages {
  private static finishedChannels: BigInt[] = [];
  private static scanning: boolean = true;
  public static checkpoints: Checkpoints[] = [];

  /**
   * Return whether the specified channel has finished scanning.
   * If no channel has been specified, returns whether the scanner is done completely.
   *
   * @param id Discord snowflake for the channel
   */
  public static isFinished(id: BigInt|null = null) {
    if(id === null) return !ScanMessages.scanning;
    return ScanMessages.finishedChannels.indexOf(id) > -1;
  }

  /**
   * Start a scan of all channels on the server:
   * - Obtain a list of all channels
   * - Load in the previous message counts
   * - Scan all text channels (delegated to ScanMessages#scanChannel)
   * - Keep track of finished channels
   *
   * @returns Promise<void>
   */
  public static async scan(): Promise<void> {
    // Get a list of all channels
    const channels = await Discord.getBot().helpers.getChannels(BigInt(Configure.get('discord_guild', 0)));

    // Execute scan
    Logger.info('Starting scan of all messages (this may take a long time)...');
    for await(const entry of channels[Symbol.iterator]()) {
      // Skip categories
      if(entry[1].type === 4) continue;

      // Skip voice channels
      if(entry[1].type === 2) continue;

      // Get the last known checkpoint from database or set to 0
      const checkpoint: ChannelCheckpointEntry = await ChannelCheckpoint().findOne(
        { channel: entry[0].toString() }
      ) as ChannelCheckpointEntry;
      if(checkpoint) {
        ScanMessages.checkpoints.push({
          channel: entry[0],
          checkpoint: checkpoint.checkpoint ? BigInt(checkpoint.checkpoint) : 0n
        });
      } else {
        ScanMessages.checkpoints.push({
          channel: entry[0],
          checkpoint: 0n
        });
      }

      // Scan the channel
      Logger.debug(`Scanning "#${entry[1].name}"...`);
      await ScanMessages.scanChannel(entry[0]);
      ScanMessages.finishedChannels.push(entry[0]);
    }
    ScanMessages.scanning = false;
    Logger.info('Finished scan!');
  }

  /**
   * Scan a single channel in batches of 100
   * Update Redis when a batch has completed
   *
   * @param id Discord snowflake of the channel to scan
   * @returns Promise<void>
   */
  private static async scanChannel(id: BigInt): Promise<void> {
    while(true) {
      // Find the current checkpoint
      const checkpoint = ScanMessages.checkpoints.find((checkpoint: Checkpoints) => checkpoint.channel === id);
      if(!checkpoint) {
        Logger.error(`No checkpoint for channel "${id}" (this is most likely a bug)`);
        return;
      }

      // Get the next 100 messages in the channel
      let messages;
      try {
        messages = await Discord.getBot().helpers.getMessages(id, {
          after: checkpoint.checkpoint,
          limit: 100
        });
      } catch(e) {
        Logger.error(`Could not scan channel: ${e.message}`, e.stack);
        return;
      }

      // Skip channels with no messages
      if(messages.array().length === 0) return;

      // Loop over each message and process it
      for (const message of messages.array()) {
        // Increase counters
        await ScanMessages.increaseMemberCount(message.authorId, message.timestamp);
        await ScanMessages.increaseChannelCount(message.authorId, message.channelId);

        // Add activity for Druid
        await Activity.add(message.authorId, message.channelId, new Date(message.timestamp));
      }

      checkpoint.checkpoint = messages.array()[0].id;
      await ScanMessages.updateCheckpoint(id, messages.array()[0].id);

      // Break when we finished scanning the channel
      if(messages.array().length < 100) break;
      Logger.debug(`Scanning next 100 messages...`);
    }
  }

  /**
   * Increase the message counter for a specified member
   *
   * @param member
   * @param last_seen
   * @returns Promise<void>
   */
  public static async increaseMemberCount(member: BigInt, last_seen: number): Promise<void> {
    // Get the current entry from the database
    const entry: GuildMemberEntry = await GuildMember().findOne({
      snowflake: member.toString()
    }) as GuildMemberEntry;

    // Insert new entry if none found
    if(!entry) {
      const user = await findUserById(member);

      await GuildMember().insertOne({
        snowflake: member.toString(),
        messages: 1,
        joined_at: new Date(user ? user.joinedAt : 0),
        last_seen: new Date(last_seen),
      });
      return;
    }

    // Increment the counter by one and update the last seen if need be
    // TODO: Find out why $inc doesn't work
    const date = new Date(last_seen);
    if(entry.last_seen > date) {
      await GuildMember().updateOne(
        { snowflake: member.toString() },
        { messages: entry.messages + 1 }
      );
    } else {
      await GuildMember().updateOne(
        { snowflake: member.toString() },
        { messages: entry.messages + 1, last_seen: date }
      );
    }
  }

  /**
   * Increase the count of channel activity by a user
   *
   * @param member Discord snowflake for the user sending the message
   * @param channel Discord snowflake for the channel
   * @returns Promise<void>
   */
  public static async increaseChannelCount(member: BigInt, channel: BigInt): Promise<void> {
    // Get the current entry from the database
    const entry: ChannelMemberMessagesEntry = await ChannelMemberMessages().findOne({
      channel: channel.toString(),
      member: member.toString(),
    }) as ChannelMemberMessagesEntry;

    // Insert new entry if none found
    if(!entry) {
      await ChannelMemberMessages().insertOne({
        channel: channel.toString(),
        member: member.toString(),
        count: 1,
      });
      return;
    }

    // Increment the counter by one
    // TODO: Find out why $inc doesn't work
    await ChannelMemberMessages().updateOne(
      { channel: channel.toString(), member: member.toString() },
      { count: entry.count + 1 }
    );
  }

  /**
   * Update the checkpoint for a channel
   *
   * @param channel
   * @param checkpoint
   * @returns Promise<void>
   */
  public static async updateCheckpoint(channel: BigInt, checkpoint: BigInt): Promise<void> {
    // Get the current entry from the database
    const entry: ChannelCheckpointEntry = await ChannelCheckpoint().findOne(
      { channel: channel.toString() }
    ) as ChannelCheckpointEntry;

    // Insert new entry if none found
    if(!entry) {
      await ChannelCheckpoint().insertOne({
        channel: channel.toString(),
        checkpoint: checkpoint.toString(),
      });
      return;
    }

    // Update the existing entry
    await ChannelCheckpoint().updateOne(
      { channel: channel.toString() },
      { checkpoint: checkpoint.toString() }
    );
  }
}
