import { Logger, cron, Configure, Discord } from "../../deps.ts";

export class DruidMembercountIngest {
  private isUpdating = false;
  private spec: any = null;

  public constructor() {
    cron('0 * * * *', async () => { await this.update(); });
  }

  /**
   * Obtain the current member count in the server and upload that to Druid
   *
   * @returns Promise<void>
   */
  private async update(): Promise<void> {
    // Make sure we don't double-update
    if(this.isUpdating === true) return;
    this.isUpdating = true;

    // Create task in Druid
    Logger.debug(`Uploading Membercount to Druid...`);
    await this.createDruidTask();
    Logger.debug(`Membercount uploaded to Druid!`);

    // Mark as finished
    this.isUpdating = false;
  }

  /**
   * Create tasks in Druid to import data
   *
   * @returns Promise<boolean>
   */
  private async createDruidTask(): Promise<void> {
    // Make sure all Druid vars are set
    if(Configure.get('druid_host') === null) throw Error('Druid host not defined!');
    if(Configure.get('druid_membercount_dataset') === null) throw Error('Druid dataset not defined!');

    // Load in the spec if need be
    if(this.spec === null) {
      this.spec = await Deno.readTextFile(`${Deno.cwd()}/src/extra/druid-membercount-spec.json`);
      if(!this.spec) throw Error('No Druid spec for "member count" could be found!');
      this.spec = this.spec
        .replace('{{DRUID_DATASET}}', Configure.get('druid_membercount_dataset'))
    }

    // Get the current time
    const now = new Date().toISOString();

    // Get guild member count
    const guild = await Discord.getBot().helpers.getGuild(BigInt(Configure.get('discord_guild', 0)), { counts: true });
    if(!guild) {
      Logger.error(`Could not update member count: Guild not found! (this is a bug!)`);
      return;
    }

    // Add our data to the spec
    const spec = this.spec
      .replace('{{TIME}}', now)
      .replace('{{COUNT}}', guild.approximateMemberCount);

    try {
      const resp = await fetch(`${Configure.get('druid_host') as string}/druid/indexer/v1/task`, {
        method: 'POST',
        body: spec,
        headers: {
          'Content-Type': 'application/json'
        }
      });
      if(!resp.ok === true) throw Error(`Received non-OK status: "${resp.statusText}"`);
    } catch(e) {
      Logger.error(`Could not create Druid task for member count (some data may go lost): ${e.message}`, e.stack);
    }
  }
}
