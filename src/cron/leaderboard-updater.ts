import { Logger, cron } from "../../deps.ts";
import { ScanMessages } from "../util/scan-messages.ts";
import { calculateScore } from "../util/calculateScore.ts";
import { Scores } from "../schema/scores.ts";
import { GuildMember, Schema as MemberEntry } from "../schema/guild-member.ts";
import { getMembershipDays } from "../util/get-membership-days.ts";
import { Leaderboard } from "../leaderboard.ts";

export class LeaderboardUpdater {
  private isUpdating = false;

  public constructor() {
    this.update();
    cron('0 * * * *', async () => {
      await this.update();
    });
  }

  /**
   * Obtain each member from the database and set their scores.
   *
   * @returns Promise<void>
   */
  private async update(): Promise<void> {
    // Make sure we don't double-update
    if(this.isUpdating === true) return;
    if(!ScanMessages.isFinished()) return;
    this.isUpdating = true;

    // Update the scores for each member
    Logger.debug(`Updating Scores...`);
    const members: MemberEntry[] = await GuildMember().find() as MemberEntry[];
    for(const member of members) {
      const membershipDays = getMembershipDays(member.joined_at, member.last_seen);
      const score = calculateScore(0, member.messages, membershipDays);
      await Scores().updateOne(
        { snowflake: member.snowflake },
        {
          messages: member.messages,
          membership_days: membershipDays,
          reputation: 0,
          total_score: score,
        },
        { upsert: true }
      );
    }
    Logger.debug(`Scores updated!`);

    // Update the leaderboard itself
    Logger.debug(`Updating Leaderbord...`);
    await Leaderboard.update();
    Logger.debug(`Leaderboard updated!`);

    // Mark as finished
    this.isUpdating = false;
  }
}
