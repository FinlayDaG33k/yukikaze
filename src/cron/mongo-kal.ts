import { Logger, cron } from "../../deps.ts";
import { GuildMember } from "../schema/guild-member.ts";

export class MongoKal {
  public constructor() {
    cron('*/5 * * * *', async () => {
      await MongoKal.update();
    });
  }

  /**
   * Send a quick find query to Mongo to keep the database connection alive
   *
   * @returns Promise<void>
   */
  private static async update(): Promise<void> {
    try {
      await GuildMember().findOne({snowflake: "91616138860978176"});
    } catch(e) {
      Logger.error(`Could not properly send keep-alive: "${e.message}"`, e.stack);
    }
  }
}
