import { Logger, cron, S3, Configure  } from "../../deps.ts";
import { Activity } from "../activity.ts";

export class DruidActivityIngest {
  private instance: any = null;
  private bucket: any = null;
  private isUpdating = false;
  private spec: any = null;

  public constructor() {
    cron('*/5 * * * *', async () => { await this.update(); });
  }

  /**
   * Obtain activity chunk (500 entries) and upload it to S3
   * Then create a Druid task to import it
   * Chunks should not be too big (Druid needs to be able to download them before the next chunk is uploaded
   *
   * @returns Promise<void>
   */
  private async update(): Promise<void> {
    // Connect to S3 if we didn't yet
    if(this.bucket === null) await this.connectS3();

    // Make sure we don't double-update
    if(this.isUpdating === true) return;

    // Get a chunk to upload
    // Stop if no chunk to be uploaded
    Logger.debug(`Obtaining activity...`);
    const activity = await Activity.getChunk(1000, true);
    if(activity.length === 0) {
      Logger.debug(`No activity to be uploaded!`);
      this.isUpdating = false;
      return;
    }
    Logger.debug(`Found "${activity.length}" activity entries`);

    // Build our data in Druid-importable format
    Logger.debug(`Building data...`);
    let data = '';
    for(let entry of activity) {
      delete entry._id;
      data += JSON.stringify(entry);
      data += '\r\n';
    }

    // Upload to Garage S3
    Logger.debug(`Uploading chunk to Garage...`);
    const encoder = new TextEncoder();
    await this.bucket.putObject("activity-chunk", encoder.encode(data), {contentType: "text/plain"});
    Logger.debug(`Druid chunk has been uploaded to Garage!`);

    // Create task in Druid
    Logger.debug(`Creating Druid task...`);
    await this.createDruidTask();
    Logger.debug(`Druid task created!`);

    // Mark as finished
    this.isUpdating = false;
  }

  /**
   * Connect to Garage S3
   *
   * @returns Promise<void>
   */
  private async connectS3(): Promise<void> {
    // Make sure all Configure vars are set
    if(Configure.get('s3_endpoint') === null) throw Error('S3 Endpoint not defined!');
    if(Configure.get('s3_key_id') === null) throw Error('S3 key not defined!');
    if(Configure.get('s3_secret') === null) throw Error('S3 secret not defined!');
    if(Configure.get('s3_region') === null) throw Error('S3 region not defined!');
    if(Configure.get('s3_druid_bucket') === null) throw Error('Druid bucket not defined!');

    /// Create S3 instance connection
    this.instance = new S3({
      accessKeyID: Configure.get('s3_key_id') as string,
      secretKey: Configure.get('s3_secret') as string,
      region: Configure.get('s3_region') as string,
      endpointURL: Configure.get('s3_endpoint') as string
    });

    // Make sure instance connection was made
    if(!this.instance) throw Error('Could not get instance!');

    // Connect to bucket
    this.bucket = await this.instance.getBucket(Configure.get('s3_druid_bucket') as string);
  }

  /**
   * Create tasks in Druid to import data
   *
   * @returns Promise<boolean>
   */
  private async createDruidTask(): Promise<boolean> {
    // Make sure all configure vars are set
    if(Configure.get('druid_host') === null) throw Error('Druid host not defined!');
    if(Configure.get('druid_activity_dataset') === null) throw Error('Druid dataset not defined!');
    if(Configure.get('s3_druid_bucket') === null) throw Error('Druid bucket not defined!');

    // Load in the spec if need be
    // TODO: Make S3 Target dynamic based on configure
    if(this.spec === null) {
      this.spec = await Deno.readTextFile(`${Deno.cwd()}/src/extra/druid-activity-spec.json`);
      if(!this.spec) throw Error('No Druid spec could be found!');
      this.spec = this.spec
        .replace('{{DRUID_DATASET}}', Configure.get('druid_activity_dataset'))
        .replace('{{S3_TARGET}}', `https://${Configure.get('s3_druid_bucket')}.s3-web.finlaydag33k.nl/activity-chunk`);
    }

    let resp;
    try {
      resp = await fetch(`${Configure.get('druid_host') as string}/druid/indexer/v1/task`, {
        method: 'POST',
        body: this.spec,
        headers: {
          'Content-Type': 'application/json'
        }
      });
    } catch(e) {
      Logger.error(`Could not create Druid task (some data may go lost): ${e.message}`, e.stack);
      return false;
    }

    return resp.status === 200;
  }
}
