export interface Interaction {
  execute(): Promise<void>;
}
