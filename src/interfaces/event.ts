export interface Event {
  execute(opts: any): Promise<void>;
}
