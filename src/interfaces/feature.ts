export interface Feature {
  execute(): Promise<void>;
}
