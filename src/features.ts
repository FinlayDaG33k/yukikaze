import { Logger } from "../deps.ts";

interface IFeature {
  name: string;
  handler: string;
}

export class Features {
  private static list: IFeature[] = [];
  private static handlers: any = {};

  /**
   * Return the complete list of features
   *
   * @returns IFeature[]
   */
  public static getFeatures(): IFeature[] { return Features.list; }

  /**
   * Find a Feature by name
   *
   * @param name
   * @returns IFeature|undefined
   */
  public static getHandler(name: string): IFeature|undefined {
    return Features.list.find((feature: IFeature) => feature.name === name);
  }

  /**
   * Import a feature handler and add it to the list of handlers
   *
   * @param feature
   * @returns Promise<void>
   */
  public static async add(feature: IFeature): Promise<void> {
    try {
      // Import the feature handler
      Features.handlers[feature.handler] = await import(`file://${Deno.cwd()}/src/features/${feature.handler}.ts`)
    } catch(e) {
      Logger.error(`Could not register feature handler for "${feature}": ${e.message}`);
      return;
    }

    Features.list.push(feature);
  }

  /**
   * Run an instance of the Feature handler
   *
   * @param feature
   * @param data
   * @returns Promise<void>
   */
  public static async dispatch(feature: string, data: any = {}): Promise<void> {
    // Get the handler
    const handler = Features.getHandler(feature);
    if(!handler) {
      Logger.warning(`Feature "${feature}" does not exist! (did you register it?)`);
      return;
    }

    // Create an instance of the handler
    const controller = new Features.handlers[handler.handler][`${feature}Feature`](data);

    // Execute the handler's execute method
    try {
      await controller['execute']();
    } catch(e) {
      Logger.error(e.message);
    }
  }
}
