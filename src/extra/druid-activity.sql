SELECT
    STRING_FORMAT('%02d:00', TIME_EXTRACT(__time, 'HOUR', '{{TIMEZONE}}')) as "hours",
    COUNT(*) as "activity"
FROM "{{DATASET}}"
GROUP BY 1
