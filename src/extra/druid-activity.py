import matplotlib.pyplot as plt
import json
import sys

# Get our data
with open(sys.argv[1]) as f:
    data = json.loads(f.read())

# Create our graph
plt.figure(figsize=(20,10))
ax = plt.axes()
ax.set_yticks([25, 50, 75, 100])
ax.set_yticklabels(['Low', 'Medium', 'High', 'Peak'])
axes = plt.gca()
axes.yaxis.grid()
plt.ylabel('Average Server Activity')
plt.xlabel(f"Hour of the day ({data['timezone']})")
plt.title(f"DPM Activity per hour ({data['samples']} samples)")
plt.bar(data['hours'], data['values'], color=data['colors'])
plt.savefig(sys.argv[1].replace('.json', '.png'))
