FROM denoland/deno:ubuntu-1.24.0

# Some base stuff
WORKDIR /app
RUN mkdir /app/logs
RUN apt-get update && apt-get install -y python3 python3-pip && pip install matplotlib

# Do not use cache below this
ARG CACHEBUST=1

# Build app
ADD ./entrypoint.ts /app
ADD ./deps.ts /app
ADD ./check.ts /app
ADD ./src /app/src
RUN deno cache deps.ts
RUN deno run --allow-all check.ts

CMD ["deno", "run", "--allow-all", "entrypoint.ts"]
