# Yukikaze

Discord bot for [Das Propaganda Maschine](https://www.finlaydag33k.nl/discord).

## Made Using
[![](https://dev.finlaydag33k.nl/3rd-party-logos/Deno-64x64.png)](https://deno.land/)
[![](https://dev.finlaydag33k.nl/3rd-party-logos/JetBrains%20IntelliJ-64x64.png)](https://www.jetbrains.com/idea/)
[![](https://dev.finlaydag33k.nl/3rd-party-logos/Discordeno-64x64.png)](https://discordeno.mod.land/)
[![](https://dev.finlaydag33k.nl/3rd-party-logos/Docker-64x46.png)](https://www.docker.com/)
[![](https://dev.finlaydag33k.nl/3rd-party-logos/Redis-64x55.png)](https://redis.io/)
