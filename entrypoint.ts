import {
  Logger,
  ApplicationCommandTypes,
  ApplicationCommandOptionTypes,
  Configure,
  Discord,
  EventDispatcher,
  InteractionDispatcher,
  dango,
} from "./deps.ts";
import { Features } from "./src/features.ts";
import { DruidActivityIngest } from "./src/cron/druid-activity-ingest.ts";
import { DruidMembercountIngest } from "./src/cron/druid-membercount-ingest.ts";
import { LeaderboardUpdater } from "./src/cron/leaderboard-updater.ts";
import { MongoKal } from "./src/cron/mongo-kal.ts";
import { Intents } from "https://deno.land/x/discordeno@13.0.0/mod.ts";

// Load Configure
await Configure.load();

// Register events
Logger.info(`Registering events...`);
await Promise.all([
  EventDispatcher.add({name: 'BotReady', handler: 'bot-ready'}),
  EventDispatcher.add({name: 'GuildLoaded', handler: 'guild-loaded'}),
  EventDispatcher.add({name: 'GuildMemberAdd', handler: 'guild-member-add'}),
  EventDispatcher.add({name: 'GuildMemberRemove', handler: 'guild-member-remove'}),
  EventDispatcher.add({name: 'InteractionCreate', handler: 'interaction-create'}),
  EventDispatcher.add({name: 'MessageReactionAdd', handler: 'message-reaction-add'}),
  EventDispatcher.add({name: 'MessageReactionRemove', handler: 'message-reaction-remove'}),
  EventDispatcher.add({name: 'MessageReceive', handler: 'message-receive'}),
  EventDispatcher.add({name: 'RoleCreate', handler: 'role-create'}),
  EventDispatcher.add({name: 'RoleUpdate', handler: 'role-update'}),
  EventDispatcher.add({name: 'RoleDelete', handler: 'role-delete'}),
  EventDispatcher.add({name: 'VoiceStateUpdate', handler: 'voice-state-update'}),
]);

// Register interactions
Logger.info(`Registering interactions...`);
await Promise.all([
  InteractionDispatcher.add({
    name: 'wikirace',
    description: 'Generate a random wikirace challenge for you',
    type: ApplicationCommandTypes.ChatInput,
    handler: 'wikirace'
  }),
  /*
  InteractionDispatcher.add({
    name: 'minecraft',
    description: 'Interact with the DPM Minecraft server',
    type: ApplicationCommandTypes.ChatInput,
    options: [
      {
        type: ApplicationCommandOptionTypes.SubCommand,
        name: 'whitelist',
        description: 'Add a user to the whitelist',
        required: false,
        autocomplete: false,
        options: [
          {
            type: ApplicationCommandOptionTypes.String,
            name: 'username',
            description: 'The minecraft username to add',
            required: true,
            autocomplete: false
          }
        ]
      }
    ],
    handler: 'minecraft'
  }),
  */
  InteractionDispatcher.add({
    name: 'profile',
    description: 'Lookup profiles on DPM',
    type: ApplicationCommandTypes.ChatInput,
    options: [
      {
        type: ApplicationCommandOptionTypes.User,
        name: 'user',
        description: 'The user to look up',
        required: false,
        autocomplete: false
      }
    ],
    handler: 'profile'
  }),
  InteractionDispatcher.add({
    name: 'activity',
    description: 'Show a graph with the average activity in the server per hour',
    type: ApplicationCommandTypes.ChatInput,
    options: [
      {
        type: ApplicationCommandOptionTypes.String,
        name: 'timezone',
        description: 'Specify a timezone (or country or city or state)',
        required: false,
        autocomplete: false
      }
    ],
    handler: 'activity'
  }),
  InteractionDispatcher.add({
    name: 'leaderboard',
    description: 'Get the current leaderboard standings',
    type: ApplicationCommandTypes.ChatInput,
    options: [
      {
        type: ApplicationCommandOptionTypes.User,
        name: 'user',
        description: 'Specify the user of whom to see the position',
        required: false,
        autocomplete: false
      }
    ],
    handler: 'leaderboard',
  }),
  InteractionDispatcher.add({
    name: 'say',
    description: 'Make Yukikaze say something',
    type: ApplicationCommandTypes.ChatInput,
    options: [
      {
        type: ApplicationCommandOptionTypes.String,
        name: 'message',
        description: 'Message to say',
        required: true,
        autocomplete: false,
      },
      {
        type: ApplicationCommandOptionTypes.Channel,
        name: 'channel',
        description: 'Specify the channel where to send the message',
        required: false,
        autocomplete: false,
      }
    ],
    handler: 'say',
  }),
  InteractionDispatcher.add({
    name: 'channel',
    description: 'Easily find a channel in the vast list of channels',
    type: ApplicationCommandTypes.ChatInput,
    options: [
      {
        type: ApplicationCommandOptionTypes.Channel,
        name: 'channel',
        description: 'The channel you want to find',
        required: true,
        autocomplete: false,
      }
    ],
    handler: 'channel',
  }),
]);

// Register features
Logger.info(`Registering features...`);
await Promise.all([
  Features.add({name: 'SelfRoleAssign', handler: 'self-role-assign'}),
  Features.add({name: 'SelfRoleRemove', handler: 'self-role-remove'})
]);

// Connect to Mongo
Logger.info(`Connecting to Mongo...`);
await dango.connect(Configure.get('mongo_uri', 'mongodb://localhost:27017'));

// Prepare bot for connection
Logger.info('Preparing to connect to Discord...');
const discord = new Discord({
  token: Configure.get('discord_token', ''),
  intents:
    Intents.Guilds |
    Intents.GuildMessages |
    Intents.DirectMessages |
    Intents.GuildMembers |
    Intents.GuildMessageReactions |
    Intents.GuildVoiceStates
  ,
  botId: BigInt(Configure.get('discord_id') ?? 0),
  withCache: false,
});

Logger.info(`Connecting to Discord...`);
await discord.start();

Logger.info(`Starting cron tasks...`);
new DruidActivityIngest();
new DruidMembercountIngest();
new LeaderboardUpdater();
new MongoKal();
